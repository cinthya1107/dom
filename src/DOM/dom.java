package DOM;

import java.io.File;

import java.io.IOException;

import java.util.Scanner;

import javax.print.Doc;

import javax.xml.parsers.DocumentBuilder;

import javax.xml.parsers.DocumentBuilderFactory;

import javax.xml.parsers.ParserConfigurationException;

import javax.xml.transform.Result;

import javax.xml.transform.Source;

import javax.xml.transform.Transformer;

import javax.xml.transform.TransformerConfigurationException;

import javax.xml.transform.TransformerException;

import javax.xml.transform.TransformerFactory;

import javax.xml.transform.TransformerFactoryConfigurationError;

import javax.xml.transform.dom.DOMSource;

import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

import org.w3c.dom.Element;

import org.w3c.dom.Node;

import org.w3c.dom.NodeList;

import org.w3c.dom.Text;

import org.xml.sax.SAXException;

public class dom {

	public static Document doc;

	static String fichero = "kimetsu.xml";

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		boolean continuar = true;

		while (continuar) {

			System.out.println(

					"Introduzca su eleccion \n 1)Leer \n 2)Insertar datos \n 3)Borrar datos \n 4)Salir");

			int eleccion = sc.nextInt();

			if (eleccion == 1) {

				Leer();

			}

			if (eleccion == 2) {

				sc.nextLine();

				System.out.println("Inserte la ID de la habilidad");

				String ID = sc.nextLine();

				System.out.println("Inserte  el nombre de la habilidad");

				String nombre = sc.nextLine();

				Modificar(ID, nombre);

			}

			if (eleccion == 3) {

				sc.nextLine();

				System.out.println("Inserte la ID de la habilidad que quieres borrar");

				String nombreBorrar = sc.nextLine();

				try {

					Borrar(nombreBorrar);

				} catch (ParserConfigurationException e) {

					e.printStackTrace();

				} catch (SAXException e) {

					e.printStackTrace();

				} catch (IOException e) {

					e.printStackTrace();

				}

			}

			if (eleccion == 4) {

				continuar = false;

			}

		}

	}

	public static void Leer() {

		try {

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

			DocumentBuilder builder = dbf.newDocumentBuilder();

			Document doc = builder.parse(new File(fichero));

			System.out.println("Elemento raiz: " + doc.getDocumentElement().getNodeName());

			NodeList alumnos = doc.getElementsByTagName("habilidad");

			for (int i = 0; i < alumnos.getLength(); i++) {

				Node alumno = alumnos.item(i);

				if (alumno.getNodeType() == Node.ELEMENT_NODE) {

					Element elemento = (Element) alumno;

					NodeList nodo = elemento.getElementsByTagName("ID").item(0).getChildNodes();

					Node valornodo = (Node) nodo.item(0);

					System.out.println("ID " + valornodo.getNodeValue());

					NodeList nodoAP = elemento.getElementsByTagName("Nombre").item(0).getChildNodes();

					Node valornodoAP = (Node) nodoAP.item(0);

					System.out.println("Nombre " + valornodoAP.getNodeValue());

				}

			}

		} catch (Exception e) {

			e.printStackTrace();

		}

	}

	public static void Modificar(String ID, String Nombre) {

		try {

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

			String fichero = "kimetsu.xml";

			DocumentBuilder builder = dbf.newDocumentBuilder();

			Document doc = builder.parse(new File(fichero));

			Element raizKimetsu = doc.createElement("habilidad");

			doc.getDocumentElement().appendChild(raizKimetsu);

			Element idElement = doc.createElement("ID");

			Text idTexto = doc.createTextNode(ID);

			raizKimetsu.appendChild(idElement);

			idElement.appendChild(idTexto);

			Element nombreElement = doc.createElement("Nombre");

			Text nombreTexto = doc.createTextNode(Nombre);

			raizKimetsu.appendChild(nombreElement);

			nombreElement.appendChild(nombreTexto);

			Source source = new DOMSource(doc);

			Result result = new StreamResult("kimetsu.xml");

			Transformer trans = TransformerFactory.newInstance().newTransformer();

			trans.transform(source, result);

		} catch (Exception e) {

			e.printStackTrace();

		}

	}

	public static void Borrar(String nombreBorrar) throws ParserConfigurationException, SAXException, IOException {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		String fichero = "kimetsu.xml";

		DocumentBuilder builder = dbf.newDocumentBuilder();

		Document doc = builder.parse(new File(fichero));

		NodeList kimetsu = doc.getElementsByTagName("habilidad");

		for (int i = 0; i < kimetsu.getLength(); i++) {

			Node habilidad = kimetsu.item(i);

			if (habilidad.getNodeType() == Node.ELEMENT_NODE) {

				Element elemento = (Element) habilidad;

				NodeList nodo = elemento.getElementsByTagName("ID").item(0).getChildNodes();

				Node valornodo = (Node) nodo.item(0);

				if (valornodo.getNodeValue().equals(nombreBorrar)) {

					habilidad.getParentNode().removeChild(habilidad);

				}

				try {

					Source source2 = new DOMSource(doc);

					Result result2 = new StreamResult("kimetsu.xml");

					Transformer trans2 = TransformerFactory.newInstance().newTransformer();

					trans2.transform(source2, result2);

				} catch (TransformerConfigurationException e) {

					e.printStackTrace();

				} catch (TransformerFactoryConfigurationError e) {

					e.printStackTrace();

				} catch (TransformerException e) {

					e.printStackTrace();

				}

			}

		}

	}
}
